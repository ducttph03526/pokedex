import React, { memo } from "react";
import { Card, Button } from "react-bootstrap";
import "./style.css";

const Item = ({ value }) => {
  console.log(value.type, "dsad");
  return (
    <div className="card-container">
      <Card className="card-pkm" onClick={() => alert("helloWorld")}>
        <Card.Img variant="top" src={value.img} />
        <Card.Body>
          {/* <Card.Title className="pkm-name">{`No: ${value.num}- ${value.name}`}</Card.Title> */}
          <Card.Title className="pkm-name">{value.name}</Card.Title>
          <Card.Text>{`Height : ${value.height}`}</Card.Text>
          <Card.Text>{`Weight: ${value.weight}`}</Card.Text>
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              textAlign: "center",
            }}
          >
            {value.type.map((v, i) => (
              <Card.Text
                key={i}
                style={{ width: 70, height: 30, margin: 10 }}
              >
                {v}
              </Card.Text>
            ))}
          </div>
        </Card.Body>
      </Card>
    </div>
  );
};

export default memo(Item);
