import React, { useEffect, useState } from "react";
import { getData } from "../../utils/request";
import { ENDPOINT } from "../../utils/contants";
import NavBar from "../../components/layout/Navbar";
import PokemonItem from "../../components/PokemonItem/index";
import "./style.css";

const HomePage = () => {
  const [limit, setLimit] = useState(10);
  const [data, setDataPokeDex] = useState([]);
  useEffect(() => {
    getDataPokeDex();
  }, []);
  function getDataPokeDex() {
    getData(`${ENDPOINT}`)
      .then((res) => setDataPokeDex(res.data.pokemon))
      .catch((err) => console.log(err));
  }
  return (
    <div className="container">
      <NavBar />
      <PokemonItem data={data} />
    </div>
  );
};
export default HomePage;
