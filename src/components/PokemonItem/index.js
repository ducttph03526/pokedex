import React, { memo } from "react";
import Item from "../Item/index";
import "./style.css";

const PokemonItem = ({ data }) => {
  console.log(data)
  return (
    <div className="container" >
      {data.map((v, i) => (
        <Item value={v} key={i} />
      ))}
    </div>
  );
};

export default memo(PokemonItem);
