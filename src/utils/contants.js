const END_POINT = "https://pokeapi.co/api/v2/pokemon/";
const ENDPOINT =
  "https://raw.githubusercontent.com/Biuni/PokemonGO-Pokedex/master/pokedex.json";
const urlImages = (pokemonIndex) => {
  return `https://github.com/PokeAPI/sprites/blob/master/sprites/pokemon/${
    pokemonIndex + 1
  }.png`;
};
function letterCapital(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
// function
export { END_POINT, urlImages, letterCapital, ENDPOINT };

