import React from 'react'
import './navbar.css'
class NavBar extends React.Component {
  render() {
    return (
      <div className='navbar nav-bar-expand-md nav-bar-dark bg-dark fixed-top'>
        <a className='title-nav-bar' href='/'>Pokedex</a>
      </div>
    )
  }
}
export default NavBar